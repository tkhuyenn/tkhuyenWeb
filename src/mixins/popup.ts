import { Component, Vue } from 'vue-property-decorator'
import { namespace } from 'vuex-class'

const xBase = namespace('xBase')
const xAuth = namespace('xAuth')

@Component
export default class PopupMixin extends Vue {
  @xBase.Action('setOpenPopup') setOpenPopup!: (data: { popupName: string; isOpen: boolean }) => void
  @xAuth.Getter('isLogin') isLogin!: boolean
  @xAuth.State('user') user!: Record<string, any>
}
