import { ModuleInterface } from '@/interface/module'
import authModule from './auth'
import landingModule from './landing'
const listModules: Record<string, ModuleInterface> = {
  authModule,
  landingModule
}

export default listModules
