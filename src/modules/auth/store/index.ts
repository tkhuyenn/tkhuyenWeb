import { Module } from 'vuex'
import xAuth from './auth'

const authStores: Record<string, Module<any, any>> = {
  xAuth
}

export default authStores
