import { ModuleInterface } from '@/interface/module'
// import authRouters from './router'
import landingRouters from './router'
import webStores from './store'

const landingModule: ModuleInterface = {
  router: landingRouters,
  stores: webStores
}

export default landingModule
