import { Module } from 'vuex'
import webStore from './webStore'

const webStores: Record<string, Module<any, any>> = {
  webStore
}

export default webStores
