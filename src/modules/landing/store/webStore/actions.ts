import Cookies from 'js-cookie'
import request from '@/plugins/request'
import { ActionTree } from 'vuex'
import { IStore } from '.'
import { AuthRepository } from '@/services/repositories/auth'
import getRepository from '@/services'
import store from '@/store'
const authRes: AuthRepository = getRepository('auth')

const actions: ActionTree<IStore, unknown> = {}
export default actions
