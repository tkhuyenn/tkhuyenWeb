import Cookies from 'js-cookie'
import { Module } from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

export interface IStore {
  cart: Array<Record<string, any>>
}

const state: IStore = {
  cart: []
}

const webStore: Module<IStore, unknown> = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

export default webStore
