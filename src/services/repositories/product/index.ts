import request from '@/plugins/request'
import { BaseRepository } from '@/services/base'

export class WebRepository extends BaseRepository {
  constructor() {
    super('product')
  }

  async getAllProduct(): Promise<any> {
    try {
      const result = await request.get(`${this.prefix}/all`)
      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }
  async getItemDetail(params: Record<string, any>): Promise<any> {
    try {
      const result = await request.get(`${this.prefix}/detail`, { params })
      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }
  async createClock(data: Record<string,any>): Promise<any> {
    try {
      const result = await request.post(`${this.prefix}/create-clock`, data)
      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }
  async createProvider(data: Record<string, any>): Promise<any> {
    try {
      const result = await request.post(`${this.prefix}/create-provider`, data)
      console.log(result, 'abc')

      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }
  async createMaterial(data: Record<string, any>): Promise<any> {
    try {
      const result = await request.post(`${this.prefix}/create-material`, data)
      console.log(result, 'abc')

      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }
  async createClockType(data: Record<string, any>): Promise<any> {
    try {
      const result = await request.post(`${this.prefix}/create-clocktype`, data)
      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async getType(params: Record<string, any>): Promise<any> {
    try {
      const result = await request.get(`${this.prefix}/all`, { params })
      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}
