import request from '@/plugins/request'
import { BaseRepository } from '@/services/base'

export class AccountRepository extends BaseRepository {
  constructor() {
    super('account')
  }

  async getAllItemInCart(): Promise<any> {
    try {
      const result = await request.get(`${this.prefix}/cart/all`)
      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async getAccountDetail(): Promise<any> {
    try {
      const result = await request.get(`${this.prefix}/detail`)
      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async pushItemIntoCart(data: Record<string, any>): Promise<any> {
    try {
      const result = await request.post(`${this.prefix}/cart/add`, data)
      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async updateQuantity(data: Record<string, any>): Promise<any> {
    try {
      const result = await request.put(`${this.prefix}/cart/update`, data)
      return Promise.resolve(result.data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }


}
