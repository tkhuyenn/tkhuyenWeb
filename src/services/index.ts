import { WebRepository } from './repositories/product/index'
import { AuthRepository } from './repositories/auth'
import { SystemRepository } from './repositories/system'
import { AccountRepository } from './repositories/account'

type RepositoryName = 'auth' | 'system' | 'product' | 'account'
export default function getRepository(name: RepositoryName): any {
  switch (name) {
    case 'auth':
      return new AuthRepository()
    case 'product':
      return new WebRepository()
    case 'account':
      return new AccountRepository()
    default:
      return null
  }
}
