import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import MainLayout from '@/components/layout/MainLayout.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: MainLayout,
    props: { header: 'MainNavbar' },
    children: [
      {
        path: '/',
        name: 'Home',
        component: () => import('../modules/landing/view/Home.vue')
      },
      {
        path: '/detail/:_id',
        name: 'Detail',
        component: () => import('../modules/landing/view/ItemDetail.vue')
      },
      {
        path: '/cart',
        name: 'Cart',
        component: () => import('../modules/landing/view/Cart.vue')
      },
      {
        path: '/backoffice',
        name: 'Manager',
        component: () => import('../modules/landing/view/Manager.vue')
      },
      {
        path: '*',
        name: 'NotFound',
        component: () => import('@/views/NotFound.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
